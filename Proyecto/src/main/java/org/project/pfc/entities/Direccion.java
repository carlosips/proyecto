package org.project.pfc.entities;

// Generated 25-abr-2016 11:55:15 by Hibernate Tools 3.4.0.CR1

import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Direccion generated by hbm2java
 */
public class Direccion implements java.io.Serializable {

	private Integer id;
	private String provincia;
	private String localidad;
	private String calle;
	private Short numero;
	
	@JsonIgnore
	private transient Set edificios = new HashSet(0);
	
	@JsonIgnore
	private transient Set fabricantes = new HashSet(0);
	
	@JsonIgnore
	private transient Set empresas = new HashSet(0);

	public Direccion() {
	}

	public Direccion(String provincia, String localidad, String calle) {
		this.provincia = provincia;
		this.localidad = localidad;
		this.calle = calle;
	}

	public Direccion(String provincia, String localidad, String calle,
			Short numero, Set edificios, Set fabricantes, Set empresas) {
		this.provincia = provincia;
		this.localidad = localidad;
		this.calle = calle;
		this.numero = numero;
		this.edificios = edificios;
		this.fabricantes = fabricantes;
		this.empresas = empresas;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getProvincia() {
		return this.provincia;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	public String getLocalidad() {
		return this.localidad;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

	public String getCalle() {
		return this.calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public Short getNumero() {
		return this.numero;
	}

	public void setNumero(Short numero) {
		this.numero = numero;
	}

	@JsonIgnore
	public Set getEdificios() {
		return this.edificios;
	}

	public void setEdificios(Set edificios) {
		this.edificios = edificios;
	}

	@JsonIgnore
	public Set getFabricantes() {
		return this.fabricantes;
	}

	public void setFabricantes(Set fabricantes) {
		this.fabricantes = fabricantes;
	}

	@JsonIgnore
	public Set getEmpresas() {
		return this.empresas;
	}

	public void setEmpresas(Set empresas) {
		this.empresas = empresas;
	}

}
