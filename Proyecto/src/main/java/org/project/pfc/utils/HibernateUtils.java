package org.project.pfc.utils;

import org.slf4j.*;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.springframework.beans.factory.annotation.Autowired;

public class HibernateUtils {

	@Autowired
    private static SessionFactory sessionFactory = getSessionFactory();
	
    private Logger logger = LoggerFactory.getLogger(HibernateUtils.class);

    private static SessionFactory buildSessionFactory() {
        try {
            // Create the SessionFactory from hibernate.cfg.xml
        	Configuration configuration = new Configuration();
        	configuration.configure();
        	ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
        	        .applySettings(configuration.getProperties()).build();
        	SessionFactory sessionFactory = configuration
        	        .buildSessionFactory(serviceRegistry);

        	return sessionFactory;
        	
            /*return new Configuration().configure().buildSessionFactory(
			    new StandardServiceRegistryBuilder().build() );*/
        }
        catch (Throwable ex) {
            // Make sure you log the exception, as it might be swallowed
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static SessionFactory getSessionFactory() {
    	if(sessionFactory == null) 
    		sessionFactory = buildSessionFactory();
        return sessionFactory;
    }

}
