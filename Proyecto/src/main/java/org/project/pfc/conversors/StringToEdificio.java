package org.project.pfc.conversors;

import java.beans.PropertyEditorSupport;

import org.hibernate.Session;
import org.project.pfc.entities.Edificio;
import org.project.pfc.utils.HibernateUtils;
import org.springframework.core.convert.converter.Converter;

public class StringToEdificio  implements Converter<String, Edificio> {
	
	/*public void setAsText(String text) {
		System.out.println(text);
		Session session = HibernateUtils.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Edificio edif = (Edificio) session.get(Edificio.class, Integer.parseInt(text));
		session.getTransaction().commit();
		setValue(edif);
	}*/

	@Override
	public Edificio convert(String text) {
		Session session = HibernateUtils.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Edificio edif = (Edificio) session.get(Edificio.class, Integer.parseInt(text));
		session.getTransaction().commit();
		return edif;
	}
}
