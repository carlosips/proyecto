package org.project.pfc.conversors;

import java.beans.PropertyEditorSupport;

import org.hibernate.Session;
import org.project.pfc.entities.Edificio;
import org.project.pfc.entities.Sustancias;
import org.project.pfc.utils.HibernateUtils;
import org.springframework.core.convert.converter.Converter;

public class StringToSustancias implements Converter<String, Sustancias>  {

	/*public void setAsText(String text) {
		System.out.println(text);
		Session session = HibernateUtils.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Sustancias sust = (Sustancias) session.get(Sustancias.class, text);
		session.getTransaction().commit();
		setValue(sust);
	}*/

	@Override
	public Sustancias convert(String text) {
		Session session = HibernateUtils.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Sustancias sust = (Sustancias) session.get(Sustancias.class, text);
		session.getTransaction().commit();
		return sust;
	}
}
