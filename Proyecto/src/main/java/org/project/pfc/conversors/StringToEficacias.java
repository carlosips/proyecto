package org.project.pfc.conversors;

import org.hibernate.Session;
import org.project.pfc.entities.Eficacias;
import org.project.pfc.utils.HibernateUtils;
import org.springframework.core.convert.converter.Converter;

public class StringToEficacias implements Converter<String, Eficacias> {

	@Override
	public Eficacias convert(String text) {
		Session session = HibernateUtils.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Eficacias fab = (Eficacias) session.get(Eficacias.class, text);
		session.getTransaction().commit();
		return fab;
	}
}