package org.project.pfc.conversors;

import java.beans.PropertyEditorSupport;

import org.hibernate.Session;
import org.project.pfc.entities.Fabricante;
import org.project.pfc.utils.HibernateUtils;
import org.springframework.core.convert.converter.Converter;

public class StringToFabricante implements Converter<String, Fabricante> {

	/*public void setAsText(String text) {
		Session session = HibernateUtils.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Fabricante fab = (Fabricante) session.get(Fabricante.class, Integer.parseInt(text));
		session.getTransaction().commit();
		setValue(fab);
	}*/

	@Override
	public Fabricante convert(String text) {
		Session session = HibernateUtils.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Fabricante fab = (Fabricante) session.get(Fabricante.class, Integer.parseInt(text));
		session.getTransaction().commit();
		return fab;
	}
}
