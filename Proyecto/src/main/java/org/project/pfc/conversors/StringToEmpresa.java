package org.project.pfc.conversors;

import org.hibernate.Session;
import org.project.pfc.entities.Empresa;
import org.project.pfc.utils.HibernateUtils;
import org.springframework.core.convert.converter.Converter;

public class StringToEmpresa  implements Converter<String, Empresa> {

	@Override
	public Empresa convert(String id) {
		Session session = HibernateUtils.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Empresa emp = (Empresa) session.get(Empresa.class, Integer.parseInt(id));
		session.getTransaction().commit();
		return emp;
	}

}
