package org.project.pfc.conversors;

import org.hibernate.Session;
import org.project.pfc.entities.Extintores;
import org.project.pfc.utils.HibernateUtils;
import org.springframework.core.convert.converter.Converter;

public class StringToExtintores implements Converter<String, Extintores> {

	@Override
	public Extintores convert(String id) {
		Session session = HibernateUtils.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Extintores ext = (Extintores) session.get(Extintores.class, Integer.parseInt(id));
		session.getTransaction().commit();
		return ext;
	}

}
