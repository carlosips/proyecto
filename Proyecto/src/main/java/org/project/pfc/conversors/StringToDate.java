package org.project.pfc.conversors;

//import java.beans.PropertyEditorSupport;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.core.convert.converter.Converter;

public class StringToDate implements Converter<String, Date> {

	
	/*public void setAsText(String text) {
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		Date date = new Date();
		try {
			date = formatter.parse(text);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		setValue(date);
	}*/

	@Override
	public Date convert(String text) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		try {
			date = formatter.parse(text);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}

	
}
