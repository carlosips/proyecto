package org.project.pfc.ifc;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.TreeMap;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;

import org.slf4j.*;
import org.project.pfc.controllers.MainController;

import Jama.Matrix;
import ifc2x3javatoolbox.demo.IfcSpatialStructure;
import ifc2x3javatoolbox.ifcmodel.IfcModel;
import ifc2x3javatoolbox.ifc2x3tc1.ClassInterface;
import ifc2x3javatoolbox.ifc2x3tc1.DOUBLE;
import ifc2x3javatoolbox.ifc2x3tc1.IfcAxis2Placement;
import ifc2x3javatoolbox.ifc2x3tc1.IfcAxis2Placement3D;
import ifc2x3javatoolbox.ifc2x3tc1.IfcBuildingElement;
import ifc2x3javatoolbox.ifc2x3tc1.IfcBuildingElementProxy;
import ifc2x3javatoolbox.ifc2x3tc1.IfcBuildingStorey;
import ifc2x3javatoolbox.ifc2x3tc1.IfcCartesianPoint;
import ifc2x3javatoolbox.ifc2x3tc1.IfcDirection;
import ifc2x3javatoolbox.ifc2x3tc1.IfcElement;
import ifc2x3javatoolbox.ifc2x3tc1.IfcFireSuppressionTerminalType;
import ifc2x3javatoolbox.ifc2x3tc1.IfcFlowTerminal;
import ifc2x3javatoolbox.ifc2x3tc1.IfcGloballyUniqueId;
import ifc2x3javatoolbox.ifc2x3tc1.IfcLabel;
import ifc2x3javatoolbox.ifc2x3tc1.IfcLengthMeasure;
import ifc2x3javatoolbox.ifc2x3tc1.IfcLocalPlacement;
import ifc2x3javatoolbox.ifc2x3tc1.IfcObject;
import ifc2x3javatoolbox.ifc2x3tc1.IfcObjectPlacement;
import ifc2x3javatoolbox.ifc2x3tc1.IfcOwnerHistory;
import ifc2x3javatoolbox.ifc2x3tc1.IfcPolyline;
import ifc2x3javatoolbox.ifc2x3tc1.IfcProduct;
import ifc2x3javatoolbox.ifc2x3tc1.IfcRelContainedInSpatialStructure;
import ifc2x3javatoolbox.ifc2x3tc1.IfcRelDefinesByType;
import ifc2x3javatoolbox.ifc2x3tc1.IfcRepresentation;
import ifc2x3javatoolbox.ifc2x3tc1.IfcRepresentationItem;
import ifc2x3javatoolbox.ifc2x3tc1.IfcRepresentationMap;
import ifc2x3javatoolbox.ifc2x3tc1.IfcRoot;
import ifc2x3javatoolbox.ifc2x3tc1.IfcShapeRepresentation;
import ifc2x3javatoolbox.ifc2x3tc1.LIST;
import ifc2x3javatoolbox.ifc2x3tc1.STRING;


public class IfcHandler {
	
	private IfcModel ifcModel;
	private String filePath;
	
	public static final String BUILDING_FOLDER = System.getProperty("user.dir") + "/files/";
	public static final String IFCCONVERT_FOLDER = System.getProperty("user.dir") + "/res/";
	public static final String GEOMETRY_FOLDER = System.getProperty("user.dir") + "/src/main/resources/static/models/";
	
	private Logger logger = LoggerFactory.getLogger(IfcHandler.class);
	
	public IfcHandler(String file_path) throws Exception{
		this.filePath = file_path;
		this.ifcModel = new IfcModel();
		
		try {
			File stepFile = new File(file_path);
			this.ifcModel.readStepFile(stepFile);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	public IfcModel getIfcModel() {
		return this.ifcModel;
	}
	
	public Collection<IfcFlowTerminal> getExtinguisher() {
		
		Collection<IfcFlowTerminal> exting = new ArrayList<IfcFlowTerminal>();
		
		Collection<IfcFireSuppressionTerminalType> tipoExtintor = this.ifcModel.getCollection(IfcFireSuppressionTerminalType.class);
		for(IfcFireSuppressionTerminalType tipo : tipoExtintor) {
			for(IfcRelDefinesByType rel : tipo.getObjectTypeOf_Inverse()) {
				for (IfcObject extintor : rel.getRelatedObjects()) {
					exting.add((IfcFlowTerminal)extintor);
				}
			}
		}
		
		return exting;
	}
	
	public static boolean isValidFile(File file) {
		IfcModel model = new IfcModel();
		boolean salida = true;
		try {
			model.readStepFile(file);
		} catch (Exception e) {
			salida = false;
		}
		return salida;
	}
	
	public static String createFileNameGuid(String name) {
		String aux = name.substring(0, name.lastIndexOf("."));
		aux += "_temporal.ifc";
		return aux;
	}
	
	public static boolean isExtinguisher(IfcProduct ifc) {
		if (ifc instanceof IfcFlowTerminal) {
			ifc = (IfcFlowTerminal)ifc;
			for (ifc2x3javatoolbox.ifc2x3tc1.IfcRelDefines def : ifc.getIsDefinedBy_Inverse()) {
				if (def instanceof IfcRelDefinesByType) {
					Object type = ((IfcRelDefinesByType) def).getRelatingType();
					if (type instanceof ifc2x3javatoolbox.ifc2x3tc1.IfcFireSuppressionTerminalType) {
						return true;
					}
				}
			}
		}
		return false;
	}
	
	public boolean probandoGeometry() {
		
		/*Collection<IfcProduct> elements = this.ifcModel.getCollection(IfcProduct.class);
		HashSet set = new HashSet();
		for(IfcProduct ibe : elements) {
				set.add(ibe.getClass());
				ifc2x3javatoolbox.ifc2x3tc1.IfcLabel lbl = ibe.getName();
				if (lbl != null)
					lbl.setValue(new ifc2x3javatoolbox.ifc2x3tc1.STRING("<" + ibe.getGlobalId() + "> " + ibe.getName(), false));
		}
		for(Object o : set) {
			logger.info("Clases: -> " + o.toString());
		}*/
		
		/*try {
			File stepFile = new File("C:/Users/carlos/Documents/workspace-spring/Proyecto/Proyecto/res/DECANATO_IFC2x3_aux.ifc");
			this.ifcModel.writeStepfile(stepFile);
		} catch (Exception e) {
			e.printStackTrace();
		}*/
		//if (true)
			//return true;
		ifc2x3javatoolbox.ifc2x3tc1.IfcFlowTerminal obj = (ifc2x3javatoolbox.ifc2x3tc1.IfcFlowTerminal)this.ifcModel.getIfcObjectByID("2$qmld92D9iBm6KIRJPXBT"); //EXTINTOR
		logger.info("clase -> " + obj.getClass());
		logger.info("tipo -> " + obj.getObjectType());
		logger.info("decomposed -> " + obj.getIsDecomposedBy_Inverse());
		logger.info("defined -> " + obj.getIsDefinedBy_Inverse());
		for (Object obj2 : obj.getIsDefinedBy_Inverse()) {
			if (obj2 instanceof ifc2x3javatoolbox.ifc2x3tc1.IfcRelDefinesByType) {
				obj2 = (ifc2x3javatoolbox.ifc2x3tc1.IfcRelDefinesByType)obj2;
				logger.info("byType_relatingtype -> " + ((ifc2x3javatoolbox.ifc2x3tc1.IfcRelDefinesByType) obj2).getRelatingType());
			}
		}
		
		Collection<IfcFireSuppressionTerminalType> tipoExtintor = this.ifcModel.getCollection(IfcFireSuppressionTerminalType.class);
		for(IfcFireSuppressionTerminalType tipo : tipoExtintor) {
			for(IfcRelDefinesByType rel : tipo.getObjectTypeOf_Inverse()) {
				for (IfcObject extintor : rel.getRelatedObjects()) {
					logger.info("guid -> " + extintor.getGlobalId());
				}
			}
		}
		
		return true;
	}
	
	
	
	public String treeHTMLGenerator() {
		String arbol = "";
		IfcSpatialStructure ss = new IfcSpatialStructure(this.ifcModel);
		DefaultMutableTreeNode treeNode = ss.getSpatialStructureRoot(false);
		IfcObject obj = (IfcObject) treeNode.getUserObject();
		arbol += "<ul><li>" + obj.getName() + treeChildren(treeNode) + "</li></ul>";

		return arbol;
	}
	
	private String treeChildren(DefaultMutableTreeNode node) {
		String stringChild = "<ul>";
		for (int i = 0; i<node.getChildCount(); i++){
			DefaultMutableTreeNode nodeChild = (DefaultMutableTreeNode)node.getChildAt(i);
			
			if (nodeChild.getUserObject() instanceof IfcObject) {

				IfcObject obj = (IfcObject) nodeChild.getUserObject();
				String name = (obj.getName() != null) ? obj.getName().toString() : "unnamed";

				name = "<span>" + name + "</span>";
				if (nodeChild.isLeaf()) {
					if (obj instanceof IfcProduct) {
						if (IfcHandler.isExtinguisher((IfcProduct)obj)) {
							stringChild += "<li data-type=\"ExtinguisherUnit\" data-guid=\""+obj.getGlobalId()+"\">"+ name + "</li>";
						}else {
							stringChild += "<li data-guid=\""+obj.getGlobalId()+"\">"+ name + "</li>";
						}
					} else {
						stringChild += "<li data-guid=\""+obj.getGlobalId()+"\">"+ name + "</li>";
					}
				} else {
					stringChild += "<li data-guid=\""+obj.getGlobalId()+"\">"+ name + treeChildren(nodeChild) +"</li>";
				}
			} else if (nodeChild.getUserObject() instanceof String) {
				String name = (nodeChild.getUserObject() != null || nodeChild.getUserObject() != "") ? (String)nodeChild.getUserObject() : "unnamed";
				String spanName = "<span>" + name + "</span>";
				if (nodeChild.isLeaf()) {
					stringChild += "<li data-type=\""+name+"\">"+ spanName + "</li>";
				} else {
					stringChild += "<li data-type=\""+name+"\">"+ spanName + treeChildren(nodeChild) +"</li>";
				}
			} else {
				logger.info("object -> " + nodeChild.getUserObject().getClass());
			}
		}
		stringChild += "</ul>";
		return stringChild;
	}
	
	public String newExtinguisher(String productGuid, double globalX, double globalY, double globalZ){
		logger.info("PNTO GLOBAL -> "+globalX+" "+globalY+" "+globalZ);
		//Cojo una muestra de extintores para obtener una representación
		ArrayList<IfcFlowTerminal> example_group = (ArrayList<IfcFlowTerminal>)getExtinguisher();
		IfcFlowTerminal example = example_group.get(0);
		
		//Este es el producto "hermano" del nuevo extintor. Compartiran el mismo contenedor
		IfcProduct product = (IfcProduct)this.ifcModel.getIfcObjectByID(productGuid);
		
		IfcFlowTerminal extintor = new IfcFlowTerminal();
		extintor.setGlobalId(new IfcGloballyUniqueId(this.ifcModel.getNewGlobalUniqueId()));
		extintor.setRepresentation(example.getRepresentation());
		
		List<Double> localPoint = pointGlobalToLocal(product, globalX, globalY, globalZ);
		
		//getLocalCordinates(-1, 0, 1, product);
		IfcLengthMeasure cordX = new IfcLengthMeasure(localPoint.get(0));
		IfcLengthMeasure cordY = new IfcLengthMeasure(localPoint.get(1));
		IfcLengthMeasure cordZ = new IfcLengthMeasure(localPoint.get(2));
		LIST<IfcLengthMeasure> cordList = new LIST<IfcLengthMeasure>();
		cordList.add(cordX); cordList.add(cordY); cordList.add(cordZ);
		IfcCartesianPoint point = new IfcCartesianPoint(cordList);
		IfcAxis2Placement3D axis = new IfcAxis2Placement3D(point, null, null);
		
		//"placement" del contenedor del objeto de referencia
		IfcObjectPlacement rel_placement = ((IfcLocalPlacement) product.getObjectPlacement()).getPlacementRelTo();
		
		IfcLocalPlacement placement = new IfcLocalPlacement(
				rel_placement, axis				
			) ;
		
		getLocalCordinates(product);
		
		this.ifcModel.addIfcObject(point);
		this.ifcModel.addIfcObject(axis);
		this.ifcModel.addIfcObject(placement);
		for (IfcRelContainedInSpatialStructure contained : ((IfcElement)product).getContainedInStructure_Inverse()) {
			contained.addRelatedElements(extintor);
		}
		extintor.setObjectPlacement(placement);
		
		extintor.setName(new IfcLabel(new STRING("EXTINTOR_"+extintor.getGlobalId(), false)));
		this.setFireSuppressionType(extintor);
		this.ifcModel.addIfcObject(extintor);
		try {
			this.ifcModel.writeStepfile(new File(this.filePath));
		} catch (IOException e) {
			e.printStackTrace();
			return "";
		}
		
		return extintor.getGlobalId().toString();
	}
	
	public void setFireSuppressionType(IfcFlowTerminal extintor) {
		for(ClassInterface obj : this.ifcModel.getIfcObjects()) {
			if (obj instanceof IfcFireSuppressionTerminalType) {
				obj = (IfcFireSuppressionTerminalType)obj;
				for(IfcRelDefinesByType rel : ((IfcFireSuppressionTerminalType) obj).getObjectTypeOf_Inverse()) {
					rel.addRelatedObjects(extintor);
				}
			}
		}
	}
	
	public void getLocalCordinates(IfcProduct product) {
		IfcObjectPlacement place = product.getObjectPlacement();
		while (place != null) {
			if (place instanceof IfcLocalPlacement) {
				IfcAxis2Placement relative = ((IfcLocalPlacement) place).getRelativePlacement();
				logger.info("relative -> " + relative);
				manageAxis(relative);
				
				IfcObjectPlacement rel_placement = ((IfcLocalPlacement) place).getPlacementRelTo();
				if (rel_placement != null)
					for (IfcProduct prod : rel_placement.getPlacesObject_Inverse()) {
						logger.info("producto -> " + prod.getName() + " - " + prod.getClass());
					}
				place = rel_placement;
			} else {
				logger.info("place -> " + place);
				place = null;
			}
		}
	}
	
	private void manageAxis(IfcAxis2Placement axis) {
		if (axis instanceof IfcAxis2Placement3D) {
			if (((IfcAxis2Placement3D) axis).getAxis() != null) {
				showDirection(((IfcAxis2Placement3D) axis).getAxis());
			} else {
				logger.info(" NO AXIS");
			}

			if (((IfcAxis2Placement3D) axis).getLocation() != null) {
				showPoint(((IfcAxis2Placement3D) axis).getLocation());
			} else {
				logger.info(" NO LOCATION");
			}

			if (((IfcAxis2Placement3D) axis).getRefDirection() != null) {
				showDirection(((IfcAxis2Placement3D) axis).getRefDirection());
			}else {
				logger.info(" NO REFDIRECTION");
			}

			if (((IfcAxis2Placement3D) axis).getAxis() != null && ((IfcAxis2Placement3D) axis).getRefDirection() != null) {
				productoVectorial(((IfcAxis2Placement3D) axis).getAxis(), ((IfcAxis2Placement3D) axis).getRefDirection());
			}
		}
	}
	
	private void showDirection(IfcDirection dir) {
		String direct = "DIRECCION (";
		for (ifc2x3javatoolbox.ifc2x3tc1.DOUBLE dob : dir.getDirectionRatios()) {
			direct += dob.value + " ";
		}
		direct +=")";
		logger.info(direct);
	}
	
	private void showPoint(IfcCartesianPoint point) {
		String p = "PUNTO (";
		for (IfcLengthMeasure len : point.getCoordinates()){
			p += len.value + " ";
		}
		p +=")";
		logger.info(p);
	}
	
	private List<Double> productoVectorial(IfcDirection ejeZ, IfcDirection ejeX) {
		LIST<DOUBLE> vectorZ = ejeZ.getDirectionRatios();
		LIST<DOUBLE> vectorX = ejeX.getDirectionRatios();
		return productoVectorial(vectorZ.get(0).value, vectorZ.get(1).value, vectorZ.get(2).value, 
				vectorX.get(0).value, vectorX.get(1).value, vectorX.get(2).value);
	}
	
	private List<Double> productoVectorial(double u1, double u2, double u3, double v1, double v2, double v3) {
		double t1 = (u2 * v3) - (u3 * v2);
		double t2 = -1 * ((u1 * v3) - (u3 * v1));
		double t3 = (u1 * v2) - (u2 * v1);
		logger.info("PRODUCTO VECTORIAL -> ("+t1+", "+t2+", "+t3+")");
		List<Double> result = new ArrayList<Double>();
		result.add(t1);
		result.add(t2);
		result.add(t3);
		return result;
	}
	
	public List<Double> pointGlobalToLocal(IfcProduct objectReference, double globalX, double globalY, double globalZ){
		//objeto de referencia
		IfcObjectPlacement place = objectReference.getObjectPlacement();
		//"placement" del contenedor del objeto de referencia
		IfcObjectPlacement rel_placement = ((IfcLocalPlacement) place).getPlacementRelTo();
		ArrayList<Double> punto = new ArrayList<Double>();
		punto.add(globalX);
		punto.add(globalY);
		punto.add(globalZ);
		ArrayList<Double> puntoLocal = getLocalPoint((IfcLocalPlacement)rel_placement, punto);
		String puntoFinal = "PUNTO FINAL (";
		for(Double p : puntoLocal) {
			puntoFinal += p + " ";
		}
		puntoFinal += ")";
		logger.info(puntoFinal);
		
		return puntoLocal;
	}
	
	private ArrayList<Double> getLocalPoint(IfcLocalPlacement place, ArrayList<Double> pointGlobal) {
		ArrayList<Double> pLocal = new ArrayList<Double>();
		Matrix result;
		if (place.getPlacementRelTo() == null) {
			return pointGlobal;
		} else {
			Matrix localPoint = pointToMatrix( getLocalPoint((IfcLocalPlacement)place.getPlacementRelTo(), pointGlobal) );
			Matrix matTrans = getTransformationMatrix(place);
			Matrix pRefer = pointToMatrix(((IfcAxis2Placement3D)place.getRelativePlacement()).getLocation());
			result = matTrans.times( localPoint.minus(pRefer) );
			logger.info("MATRIZ TRANSFORMACION");
			showMatrix(matTrans);
			logger.info("PUNTO LOCAL");
			showMatrix(localPoint);
			logger.info("PUNTO REFERENCIA");
			showMatrix(pRefer);
		}
		pLocal.add(result.get(0,0));
		pLocal.add(result.get(1,0));
		pLocal.add(result.get(2,0));
		return pLocal;
	}
	
	private Matrix pointToMatrix(LIST<DOUBLE> punto) {
		Matrix p = new Matrix(3,1);
		p.set(0, 0, punto.get(0).value);
		p.set(1, 0, punto.get(1).value);
		p.set(2, 0, punto.get(2).value);
		return p;
	}
	
	private Matrix pointToMatrix(List<Double> punto) {
		Matrix p = new Matrix(3,1);
		p.set(0, 0, punto.get(0).doubleValue());
		p.set(1, 0, punto.get(1).doubleValue());
		p.set(2, 0, punto.get(2).doubleValue());
		return p;
	}
	
	private Matrix pointToMatrix(IfcCartesianPoint punto) {
		Matrix p = new Matrix(3,1);
		p.set(0, 0, punto.getCoordinates().get(0).value);
		p.set(1, 0, punto.getCoordinates().get(1).value);
		p.set(2, 0, punto.getCoordinates().get(2).value);
		return p;
	}
	
	private Matrix getTransformationMatrix(IfcLocalPlacement place) {
		Matrix trans = new Matrix(3,3);
		IfcAxis2Placement axis = place.getRelativePlacement();
		if (!(axis instanceof IfcAxis2Placement3D)) {
			return null;
		}
		if (((IfcAxis2Placement3D) axis).getAxis() != null) {
			int column = 0;
			for (ifc2x3javatoolbox.ifc2x3tc1.DOUBLE dob : ((IfcAxis2Placement3D) axis).getAxis().getDirectionRatios()) {
				trans.set(2,column, dob.value);
				++column;
			}
		} else {
			trans.set(2,0, 0);
			trans.set(2,1, 0);
			trans.set(2,2, 1);
		}


		if (((IfcAxis2Placement3D) axis).getRefDirection() != null) {
			int column = 0;
			for (ifc2x3javatoolbox.ifc2x3tc1.DOUBLE dob : ((IfcAxis2Placement3D) axis).getRefDirection().getDirectionRatios()) {
				trans.set(0,column, dob.value);
				++column;
			}
		}else {
			trans.set(0,0, 1);
			trans.set(0,1, 0);
			trans.set(0,2, 0);
		}

		if (((IfcAxis2Placement3D) axis).getAxis() != null && ((IfcAxis2Placement3D) axis).getRefDirection() != null) {
			List<Double> prod = productoVectorial(((IfcAxis2Placement3D) axis).getAxis(), ((IfcAxis2Placement3D) axis).getRefDirection());
			trans.set(1,0, prod.get(0).doubleValue());
			trans.set(1,1, prod.get(1).doubleValue());
			trans.set(1,2, prod.get(2).doubleValue());
		} else {
			trans.set(1,0, 0);
			trans.set(1,1, 1);
			trans.set(1,2, 0);
		}
		
		return trans.transpose().inverse();
	}
	
	private void showMatrix(Matrix mat) {
		for(int i = 0; i< mat.getRowDimension(); i++) {
			String row = "";
			for(int j = 0; j < mat.getColumnDimension(); j++) {
				row += mat.get(i, j) + "\t";
			}
			logger.info("------->"+row);
		}
	}
	
	public int deleteElement(String guid) {
		try {
			IfcProduct product = (IfcProduct)this.ifcModel.getIfcObjectByID(guid);
			this.ifcModel.removeIfcObject(product);
			
			for(ClassInterface obj : this.ifcModel.getIfcObjects()) {
				if (obj instanceof IfcFireSuppressionTerminalType) {
					obj = (IfcFireSuppressionTerminalType)obj;
					for(IfcRelDefinesByType rel : ((IfcFireSuppressionTerminalType) obj).getObjectTypeOf_Inverse()) {
						rel.removeRelatedObjects(product);
					}
				}
			}
			
			for (IfcRelContainedInSpatialStructure contained : ((IfcElement)product).getContainedInStructure_Inverse()) {
				contained.removeRelatedElements(product);
			}
			
			this.ifcModel.writeStepfile(new File(this.filePath));

		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
		
		return 1;
	}
}
