package org.project.pfc.controllers;

import ifc2x3javatoolbox.ifc2x3tc1.ClassInterface;
import ifc2x3javatoolbox.ifc2x3tc1.IfcFireSuppressionTerminalType;
import ifc2x3javatoolbox.ifc2x3tc1.IfcFlowTerminal;
import ifc2x3javatoolbox.ifc2x3tc1.IfcObject;
import ifc2x3javatoolbox.ifc2x3tc1.IfcObject;
import ifc2x3javatoolbox.ifc2x3tc1.IfcProduct;
import ifc2x3javatoolbox.ifc2x3tc1.IfcRelContainedInSpatialStructure;
import ifc2x3javatoolbox.ifc2x3tc1.IfcRelDecomposes;
import ifc2x3javatoolbox.ifc2x3tc1.IfcRelDefinesByType;
import ifc2x3javatoolbox.ifc2x3tc1.IfcSIUnit;
import ifc2x3javatoolbox.ifc2x3tc1.IfcSpace;
import ifc2x3javatoolbox.ifc2x3tc1.SET;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.swing.tree.DefaultMutableTreeNode;

import org.slf4j.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.hibernate.Session;
import org.project.pfc.utils.*;
import org.project.pfc.entities.*;
import org.project.pfc.ifc.*;
import org.project.pfc.ifc.*;

@Controller
@RequestMapping("/")
public class MainController {
	
	private Logger logger = LoggerFactory.getLogger(MainController.class);
	private String path = "C:/Users/carlos/Documents/workspace-spring/Proyecto/Proyecto/files/dvflAng8.ifc";
	
	
	public static final int ERROR_UPLOAD_FILE 	= 11111;
	public static final int ERROR_TYPE_FILE 	= 22222;
	public static final int ERROR_FILE_NOTFOUND = 33333;

	
	@RequestMapping("")
	public ModelAndView redirect() {
		return new ModelAndView("redirect:/inicio");
	}
	
	
	@RequestMapping("inicio")
	public String index(Model model) {
		logger.info("MainController: /");
		Session session = HibernateUtils.getSessionFactory().getCurrentSession();
		logger.info("Sesion obtenida");
		session.beginTransaction();
		logger.info("Transaccion empezada");
        List<Edificio> result = session.createQuery("from Edificio").list();
        session.close();
        model.addAttribute("edificios", result);
        
		return "inicio/main";
	}
	
	
	
	
	
	@RequestMapping(value="inicio/edificio/{edifId}")
	public String showBuilding(@PathVariable Integer edifId, Model model) {
		boolean extDefined = false;
		Session session = HibernateUtils.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Edificio edif = (Edificio) session.get(Edificio.class, edifId);
		List<Extintores> extBd = session.createQuery("from Extintores e where e.edificio = :edificio")
				.setParameter("edificio", edif)
				.list();
		session.getTransaction().commit();
		logger.info("Extitores: " + extBd.size());
		
		IfcHandler handler;
		try {
			handler = new IfcHandler(IfcHandler.BUILDING_FOLDER + edif.getArchivo());
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error", this.ERROR_FILE_NOTFOUND);
            return "control/error";
		}
		
		Collection<IfcFlowTerminal> ifc = handler.getExtinguisher();
		logger.info("Extitores IFC: " + ifc.size());
		if (ifc.size() > extBd.size()) {
			ArrayList<String> gId = new ArrayList<String>();
			//ArrayList<IfcFlowTerminal> extintores = new ArrayList<IfcFlowTerminal>();
			ArrayList<String> extintores = new ArrayList<String>();
			for (Extintores e: extBd) {
				gId.add(e.getIfcGlobalId());
			}
			for (IfcFlowTerminal i: ifc) {
				if (!gId.contains(i.getGlobalId().toString())) {
					extintores.add(i.getGlobalId().toString());
				}
			}
			model.addAttribute("extintores", extintores);
		} else {
			logger.info("Extitores Definidos");
			extDefined = true;
			model.addAttribute("extintores", extBd);
		}
		model.addAttribute("arbol", handler.treeHTMLGenerator());
		model.addAttribute("wavefront", edif.getArchivo().replace(".ifc",".obj"));
		model.addAttribute("extDefined", extDefined);
		model.addAttribute("edificio", edif);
		return "inicio/edificio";
	}
	
	
}
