package org.project.pfc.controllers;

import ifc2x3javatoolbox.ifc2x3tc1.IfcFlowTerminal;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.slf4j.*;
import org.hibernate.Session;
import org.project.pfc.entities.*;
import org.project.pfc.ifc.IfcHandler;
import org.project.pfc.utils.HibernateUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/ajax")
public class AjaxController {
	
	private Logger logger = LoggerFactory.getLogger(AjaxController.class);

	@RequestMapping(value="sustancias", method=RequestMethod.GET, produces="application/json")
	public @ResponseBody List<Sustancias> getSustancias () {
		Session session = HibernateUtils.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<Sustancias> result = session.createQuery("from Sustancias").list();
		session.getTransaction().commit();
		return result;
	}
	
	@RequestMapping(value="eficacias/{tipo}", method=RequestMethod.GET, produces="application/json")
	public @ResponseBody List<Eficacias> getEficacias (@PathVariable String tipo) {
		Session session = HibernateUtils.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<Eficacias> result = session.createQuery("from Eficacias WHERE tipo = '" + tipo + "'").list();
		session.getTransaction().commit();
		return result;
	}
	
	@RequestMapping(value="fabricantes", method=RequestMethod.GET, produces="application/json")
	public @ResponseBody List<Fabricante> getFabricantes () {
		Session session = HibernateUtils.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<Fabricante> result = session.createQuery("from Fabricante").list();
		session.getTransaction().commit();
		return result;
	}
	
	@RequestMapping(value="extintor", method=RequestMethod.GET, produces="application/json")
	public @ResponseBody Extintores getExtintor (@RequestParam("edif") String edificio, @RequestParam("guid") String guid) {
		Session session = HibernateUtils.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<Extintores> result = session.createQuery("from Extintores WHERE edificio = " + edificio + " AND ifcGlobalId = '"+guid+"'").list();
		session.getTransaction().commit();
		if (result.size() != 1) {
			return null;
		}
		
		return result.get(0);
	}
}
