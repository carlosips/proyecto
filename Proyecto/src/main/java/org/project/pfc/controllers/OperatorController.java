package org.project.pfc.controllers;

import ifc2x3javatoolbox.ifc2x3tc1.IfcFlowTerminal;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.NaturalIdLoadAccess;
import org.hibernate.Session;
import org.project.pfc.entities.Edificio;
import org.project.pfc.entities.Eficacias;
import org.project.pfc.entities.EficaciasExtintor;
import org.project.pfc.entities.Extintores;
import org.project.pfc.entities.Mantenimiento;
import org.project.pfc.entities.Usuario;
import org.project.pfc.ifc.IfcHandler;
import org.project.pfc.utils.HibernateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
@RequestMapping("/operario")
public class OperatorController {

	private Logger logger = LoggerFactory.getLogger(OperatorController.class);
	
	@RequestMapping(value="guardar-extintor", method=RequestMethod.POST)
	public ResponseEntity<String> postExtintor(@ModelAttribute("extintores") Extintores extintor, @RequestParam("eficacias[]") String[] eficacias, BindingResult result) {
		String message = "OK";
		HttpStatus status = HttpStatus.CREATED;
		if (result.hasErrors()) {
			message = "";
			status = HttpStatus.INTERNAL_SERVER_ERROR;
			for (ObjectError err : result.getAllErrors()) {
				message += err.toString() + "\n\n";
			}
		} else {
			Session session = HibernateUtils.getSessionFactory()
					.getCurrentSession();

			IfcHandler handler;
			
			try {
				Edificio edif = extintor.getEdificio();
				handler = new IfcHandler(IfcHandler.BUILDING_FOLDER + edif.getArchivo());
				Collection<IfcFlowTerminal> ifc = handler.getExtinguisher();
				
				session.beginTransaction();
				session.save(extintor);
				for(String eficacia : eficacias) {
					Eficacias ef = (Eficacias)session.get(Eficacias.class, eficacia);
					EficaciasExtintor efex = new EficaciasExtintor(ef, extintor);
					session.save(efex);
				}
				if (edif.getExtintoreses().size() == ifc.size() - 1) {
					logger.info("Todos los extintores definidos. Validando Edificio...");
					edif.setValidado(true);
					session.update(edif);
				}
				session.getTransaction().commit();
			} catch (Exception e) {
				message = e.getMessage();
				e.printStackTrace();
				status = HttpStatus.INTERNAL_SERVER_ERROR;
			}
		}
		ResponseEntity<String> response = new ResponseEntity<String>(message, status);	
		return response;
	}
	
	@RequestMapping(value="actualizar-extintor/{id}", method=RequestMethod.POST)
	public ResponseEntity<String> updateExtintor(@PathVariable("id") String id, @ModelAttribute("extintores") Extintores extintor_update, @RequestParam("eficacias[]") String[] eficacias, BindingResult result) {
		String message = "OK";
		HttpStatus status = HttpStatus.OK;
		if (result.hasErrors()) {
			message = "";
			status = HttpStatus.INTERNAL_SERVER_ERROR;
			for (ObjectError err : result.getAllErrors()) {
				message += err.toString() + "\n\n";
			}
		} else {
			Session session = HibernateUtils.getSessionFactory()
					.getCurrentSession();

			IfcHandler handler;
			
			try {
				session.beginTransaction();
				Extintores extintor = (Extintores)session.get(Extintores.class, Integer.parseInt(id));
				extintor.updateCopy(extintor_update);
				
				Set<String> new_ef_conj = new HashSet<String>(Arrays.asList(eficacias));
				Set<String> old_ef_conj = new HashSet<String>(); 
				for(EficaciasExtintor ef :  extintor.getEficaciasExtintors()) {
					if (!new_ef_conj.contains(ef.getEficacias().getNombre())) {
						session.delete(ef);
					}
					old_ef_conj.add(ef.getEficacias().getNombre());
				}
				for(String efc : new_ef_conj) {
					if (!old_ef_conj.contains(efc)) {
						Eficacias ef = (Eficacias)session.get(Eficacias.class, efc);
						EficaciasExtintor efex = new EficaciasExtintor(ef, extintor);
						session.save(efex);
					}
				}

				session.update(extintor);
				session.getTransaction().commit();
			} catch (Exception e) {
				message = e.getMessage();
				e.printStackTrace();
				status = HttpStatus.INTERNAL_SERVER_ERROR;
			}
		}
		
		ResponseEntity<String> response = new ResponseEntity<String>(message, status);	
		return response;
	}
	
	
	@RequestMapping(value="guardar-mantenimiento", method=RequestMethod.POST)
	public ResponseEntity<String> postMantenimiento(@ModelAttribute("mantenimiento") Mantenimiento mant, BindingResult result) {
		String message = "OK";
		HttpStatus status = HttpStatus.CREATED;
		
		if (result.hasErrors()) {
			message = "";
			status = HttpStatus.INTERNAL_SERVER_ERROR;
			for (ObjectError err : result.getAllErrors()) {
				message += err.toString() + "\n\n";
			}
		} else {
			Session session = HibernateUtils.getSessionFactory()
					.getCurrentSession();
			
			User user = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		    String name = user.getUsername();
		    
			try {
				session.beginTransaction();
				NaturalIdLoadAccess naturalIdentifier = session.byNaturalId(Usuario.class);
				naturalIdentifier.using("nombreUsuario", name);
				Usuario usuario = (Usuario)naturalIdentifier.load();
				mant.setUsuario(usuario);
				session.save(mant);
				session.getTransaction().commit();
			} catch (Exception e) {
				message = e.getMessage();
				e.printStackTrace();
				status = HttpStatus.INTERNAL_SERVER_ERROR;
			}
		}
		
		ResponseEntity<String> response = new ResponseEntity<String>(message, status);	
		return response;
	}
}
