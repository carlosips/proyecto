package org.project.pfc.controllers;

import ifc2x3javatoolbox.ifc2x3tc1.IfcBuildingElementProxy;
import ifc2x3javatoolbox.ifc2x3tc1.IfcFlowTerminal;
import ifc2x3javatoolbox.ifc2x3tc1.IfcProduct;
import ifc2x3javatoolbox.ifc2x3tc1.IfcRelContainedInSpatialStructure;
import ifc2x3javatoolbox.ifc2x3tc1.IfcRelDecomposes;
import ifc2x3javatoolbox.ifc2x3tc1.IfcSpace;
import ifc2x3javatoolbox.ifc2x3tc1.SET;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.*;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;
import org.project.pfc.entities.Direccion;
import org.project.pfc.entities.Edificio;
import org.project.pfc.entities.Eficacias;
import org.project.pfc.entities.EficaciasExtintor;
import org.project.pfc.entities.Extintores;
import org.project.pfc.entities.Roles;
import org.project.pfc.entities.Usuario;
import org.project.pfc.ifc.IfcHandler;
import org.project.pfc.utils.HibernateUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

@Controller
@RequestMapping("/admin")
public class AdminController {
	
	public final int ERROR_UPLOAD_FILE 	= 11111;
	public final int ERROR_TYPE_FILE 	= 22222;
	public final int ERROR_FILE_NOTFOUND = 33333;
	
	private Logger logger = LoggerFactory.getLogger(MainController.class);
	
	@RequestMapping(value="lista-usuarios", method=RequestMethod.GET)
	public String listUsers(Model model) {
		logger.info("AdminController: Lista Usuarios");	
		Session session = HibernateUtils.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<Usuario> usuarios = session.createQuery("from Usuario").list();
		session.getTransaction().commit();
		for (Usuario u : usuarios) {
			logger.info(u.getNombreUsuario());
		}
		model.addAttribute("usuarios", usuarios);
		return "admin/lista_usuarios";
	}
	
	@RequestMapping(value="borrar-usuario/{idUser}", method=RequestMethod.DELETE, produces="application/json")
	public ResponseEntity<String> deleteUser(@PathVariable Integer idUser, Model model) {
	
		Session session = HibernateUtils.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			Usuario usuario = (Usuario) session.get(Usuario.class, idUser);
			session.delete(usuario);
			session.getTransaction().commit();
			logger.info("BORRANDO USUARIO");
		} catch (Exception e) {
			logger.info("ERROR");
			session.getTransaction().rollback();
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		} 
		logger.info("BORRANDO OK");
		return ResponseEntity.ok("");
	}
	
	@RequestMapping(value="nuevo-usuario", method=RequestMethod.GET)
	public String newUserGet(Model model) {
		logger.info("AdminController: Nuevo Usuario GET");	
		model.addAttribute("usuario", new Usuario());
		model.addAttribute("roles_", Roles.getMapRoles());
		return "admin/nuevo_usuario";
	}
	
	@RequestMapping(value="nuevo-usuario", method=RequestMethod.POST)
	public String newUserPost(@ModelAttribute Usuario usuario, @RequestParam("roles_attr[]") String[] roles) {
		logger.info("AdminController: Nuevo Usuario POST");
		Set<Roles> roles_ = new HashSet();
		for (String rol : roles) {
			logger.info("ROL: "+rol);
			Roles rol_ = new Roles(usuario, rol);
			roles_.add(rol_);
		}
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		usuario.setPassword(encoder.encode(usuario.getPassword()));
		Session session = HibernateUtils.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			usuario.setRoleses(roles_);
			session.save(usuario);
			session.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			return "control/error";
		}
		return "redirect:/";
	}
	
	
	@RequestMapping(value="edit-usuario/{idUser}", method=RequestMethod.GET)
	public String editUserGet(@PathVariable Integer idUser, Model model) {
		logger.info("AdminController: Edit Usuario GET");	
		model.addAttribute("usuario", new Usuario());
		return "admin/nuevo_usuario";
	}
	
	
	@RequestMapping(value="nuevo-edificio", method=RequestMethod.GET)
	public String newBuildingGet(Model model) {
		logger.info("AdminController: Nuevo Edificio GET");	
		model.addAttribute("direccion", new Direccion());
		return "admin/nuevo_edificio";
	}
	
	@RequestMapping(value="nuevo-edificio", method=RequestMethod.POST)
	public String newBuildingPost(@RequestParam("descripcion") String descripcion,
            @RequestParam("file") MultipartFile multipartFile, Model model, @ModelAttribute Direccion direccion) {
		logger.info("MainController: Nuevo Edificio POST");
		logger.info("Working Directory = " +
	              System.getProperty("user.dir"));
		
		if (descripcion.startsWith(","))
			descripcion = descripcion.replaceFirst(",", "");
        if (!multipartFile.isEmpty()) {
            try {
            	logger.info(IfcHandler.BUILDING_FOLDER + multipartFile.getOriginalFilename());
            	logger.info(direccion.getCalle() + " " + direccion.getNumero() + " " + direccion.getLocalidad());
            	Session session = HibernateUtils.getSessionFactory().getCurrentSession();
            	String name = String.format("%s.%s", RandomStringUtils.randomAlphanumeric(8), "ifc");
            	
        		logger.info("Sesion obtenida");
        		session.beginTransaction();
        		Edificio edif = new Edificio();
        		edif.setArchivo(name);
        		edif.setDescripcion(descripcion);
        		edif.setValidado(false);
        		edif.setDireccion(direccion);
        		session.save(direccion);
        		session.save(edif);

            	File file = new File(IfcHandler.BUILDING_FOLDER + name);
            	
            	multipartFile.transferTo(file);
            	if (IfcHandler.isValidFile(file)) {
            		session.getTransaction().commit();
            		return "redirect:/admin/generar-geometria/"+edif.getId();
            	} else {
            		file.delete();
            		model.addAttribute("error", this.ERROR_TYPE_FILE);
                    return "control/error";
            	}
            	
            } catch (Exception e) {
            	model.addAttribute("error", this.ERROR_UPLOAD_FILE);
            	model.addAttribute("mensaje", e.getMessage());
                return "control/error";
            }
        } else {
        	model.addAttribute("error", this.ERROR_FILE_NOTFOUND);
            return "control/error";
        }
	}
	
	@RequestMapping(value="generar-geometria/{edifId}", method=RequestMethod.GET)
	public String newGeometryGet(@PathVariable String edifId, Model model) {
		logger.info("AdminController: Nueva Geometria GET");	
		Session session = HibernateUtils.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Edificio edif = (Edificio) session.get(Edificio.class, Integer.parseInt(edifId));
		session.getTransaction().commit();
		IfcHandler handler;
		try {
			handler = new IfcHandler(IfcHandler.BUILDING_FOLDER + edif.getArchivo());
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error", this.ERROR_FILE_NOTFOUND);
            return "control/error";
		}
		
		HashSet<String> setClases = nameToGuidFile(handler, edif.getArchivo());
		if (setClases == null) {
			return "control/error";
		}
		setClases = excludeClasses(setClases);
		model.addAttribute("clases", setClases);
		model.addAttribute("idEdif", edifId);
		model.addAttribute("descripcion", edif.getDescripcion());
		return "admin/nueva_geometria";
	}
	
	
	
	@RequestMapping(value="generar-geometria/{edifId}", method=RequestMethod.POST)
	public String newGeometryPost(@PathVariable String edifId, @RequestParam("exclude_arr[]") String[] ifcExclude, Model model) {
		logger.info("AdminController: Nueva Geometria POST");	
		Session session = HibernateUtils.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Edificio edif = (Edificio) session.get(Edificio.class, Integer.parseInt(edifId));
		
		String classList = "";
		for (String clase : ifcExclude) {
			classList += clase + " ";
		}
		edif.setExclusionGeometria(classList);
		if (!executeIfcConvert(classList, edif.getArchivo())) {
			return "control/error";
		}
		edif.setExclusionGeometria(classList);
		try {
			
			session.update(edif);
			session.getTransaction().commit();
			File stepFile = new File(IfcHandler.BUILDING_FOLDER + IfcHandler.createFileNameGuid(edif.getArchivo()));
			stepFile.delete();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "control/exito";
	}
	
	@RequestMapping(value="nuevo-extintor", method=RequestMethod.POST)
	public String postExtintor(@ModelAttribute("extintores") Extintores extintor, 
												@RequestParam("eficacias[]") String[] eficacias, 
												@RequestParam(value = "cordX") String cordX,
												@RequestParam(value = "cordY") String cordY,
												@RequestParam(value = "cordZ") String cordZ,
												@RequestParam("referenceGuid") String referenceGuid,
												BindingResult result,
												Model model) 
	{
		String message = "OK";
		HttpStatus status = HttpStatus.CREATED;
		if (cordX.startsWith(","))
			cordX = cordX.replaceFirst(",", "");
		if (cordY.startsWith(","))
			cordY = cordY.replaceFirst(",", "");
		if (cordZ.startsWith(","))
			cordZ = cordZ.replaceFirst(",", "");
		if (referenceGuid.startsWith(","))
			referenceGuid = referenceGuid.replaceFirst(",", "");
		
		
		Double cx;
		Double cy;
		Double cz;
		
		try {
			cx = Double.parseDouble(cordX);
			cy = Double.parseDouble(cordY);
			cz = Double.parseDouble(cordZ);
		} catch (Exception ex) {
			return "control/error";
		}
		

		
		if (result.hasErrors()) {
			message = "";
			status = HttpStatus.INTERNAL_SERVER_ERROR;
			for (ObjectError err : result.getAllErrors()) {
				message += err.toString() + "\n\n";
			}
		} else {
			
			
			Session session = HibernateUtils.getSessionFactory()
					.getCurrentSession();

			IfcHandler handler;
			Edificio edif = extintor.getEdificio();
			try {
				
				handler = new IfcHandler(IfcHandler.BUILDING_FOLDER + edif.getArchivo());
				String ifcGlobalId = handler.newExtinguisher(referenceGuid, cx*1000, cy*1000, cz*1000); 
				
				if (ifcGlobalId == "") {
					throw new Exception("ERROR AL MODIFICAR ARCHIVO IFC");
				}
				extintor.setIfcGlobalId(ifcGlobalId);
				session.beginTransaction();
				session.save(extintor);
				
				for(String eficacia : eficacias) {
					Eficacias ef = (Eficacias)session.get(Eficacias.class, eficacia);
					if (ef != null){
						logger.info(ef.getNombre());
						EficaciasExtintor efex = new EficaciasExtintor(ef, extintor);
						session.save(efex);
					}
					
				}

				session.getTransaction().commit();
				nameToGuidFile(handler, edif.getArchivo());
				if (!executeIfcConvert(edif.getExclusionGeometria(), edif.getArchivo())) {
					return "control/error";
				}
				File stepFile = new File(IfcHandler.BUILDING_FOLDER + IfcHandler.createFileNameGuid(edif.getArchivo()));
				stepFile.delete();
			} catch (Exception e) {
				message = e.getMessage();
				e.printStackTrace();
				status = HttpStatus.INTERNAL_SERVER_ERROR;
			}
			
			
			
		}
		ResponseEntity<String> response = new ResponseEntity<String>(message, status);	
		return "control/exito";
		
	}
	
	@RequestMapping(value="borrar-extintor", method=RequestMethod.POST)
	public String deleteExtinguiser(@RequestParam(value = "idExt") String idExt, Model model) {
		IfcHandler handler;
		Session session = HibernateUtils.getSessionFactory().getCurrentSession();
		
		Extintores extintor;
		Edificio edif;
		try {
			session.beginTransaction();
			extintor = (Extintores) session.get(Extintores.class, Integer.parseInt(idExt));
			edif = extintor.getEdificio();
			handler = new IfcHandler(IfcHandler.BUILDING_FOLDER + extintor.getEdificio().getArchivo());
			IfcProduct ext = (IfcProduct)handler.getIfcModel().getIfcObjectByID(extintor.getIfcGlobalId());
			if(IfcHandler.isExtinguisher(ext)) {
				handler.deleteElement(extintor.getIfcGlobalId());
			} else {
				model.addAttribute("message", "Se intentó borrar algo que no era un extintor");
				return "control/error";
			}
			
			session.delete(extintor);
			session.getTransaction().commit();
			
			nameToGuidFile(handler, edif.getArchivo());
			if (!executeIfcConvert(edif.getExclusionGeometria(), edif.getArchivo())) {
				model.addAttribute("message", "No se pudo generar una nueva geometría");
				return "control/error";
			}
			File stepFile = new File(IfcHandler.BUILDING_FOLDER + IfcHandler.createFileNameGuid(edif.getArchivo()));
			stepFile.delete();
		} catch(Exception e) {
			model.addAttribute("message", "Ocurrió un problema al borrar el extintor");
			return "control/error";
		}
		
		
		return "control/exito";
	}
	
	//Funciones de apoyo
	
	private HashSet<String> excludeClasses(HashSet<String> clases) {
		clases.remove("IfcBuilding");
		clases.remove("IfcOpeningElement");
		clases.remove("IfcSpace");
		clases.remove("IfcBuildingStorey");
		clases.remove("IfcSite");
		
		//Los extintores están dentro de esta clase, por la que tiene que ser excluida de 
		//las clases que se excluyen en la generación de geometria
		clases.remove("IfcFlowTerminal");

		return clases;
	}
	
	private boolean executeIfcConvert(String classList, String archivo) {
		String executable = "IfcConvert --sew-shells --entities IfcOpeningElement IfcSpace " + classList + "--exclude " + 
				IfcHandler.BUILDING_FOLDER + IfcHandler.createFileNameGuid(archivo) + " " +
				IfcHandler.GEOMETRY_FOLDER + archivo.replace(".ifc", ".obj");
		Runtime externalTool = Runtime.getRuntime();
		Process ifcConvert;
		try {
			ifcConvert = externalTool.exec(IfcHandler.IFCCONVERT_FOLDER + executable);
			ifcConvert.waitFor();
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	private HashSet<String> nameToGuidFile(IfcHandler handler, String fileName) {
		Collection<IfcProduct> elements = handler.getIfcModel().getCollection(IfcProduct.class);
		HashSet<String> setClases = new HashSet();
		for(IfcProduct ibe : elements) {
				String clase = ibe.getClass().toString();
				int index = clase.lastIndexOf(".") + 1;
				setClases.add(clase.substring(index));
				ifc2x3javatoolbox.ifc2x3tc1.IfcLabel lbl = ibe.getName();
				if (lbl != null)
					lbl.setValue(new ifc2x3javatoolbox.ifc2x3tc1.STRING(ibe.getGlobalId().toString(), false));
		}
		try {
			File stepFile = new File(IfcHandler.BUILDING_FOLDER + IfcHandler.createFileNameGuid(fileName));
			handler.getIfcModel().writeStepfile(stepFile);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return setClases;
	}
}
