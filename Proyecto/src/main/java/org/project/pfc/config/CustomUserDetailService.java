package org.project.pfc.config;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.hibernate.NaturalIdLoadAccess;
import org.hibernate.Session;
import org.project.pfc.entities.*;
import org.project.pfc.utils.HibernateUtils;

@Service
public class CustomUserDetailService implements UserDetailsService{

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		Session session = HibernateUtils.getSessionFactory().getCurrentSession();

		session.beginTransaction();
		//List<Usuario> result = session.createQuery("from Usuario Where nombre_usuario = " + username).list();
		NaturalIdLoadAccess naturalIdentifier = session.byNaturalId(Usuario.class);
		naturalIdentifier.using("nombreUsuario", username);
		Usuario user = (Usuario)naturalIdentifier.load();
		session.close();

		List<GrantedAuthority> authorities = buildUserAuthority(user.getRoleses());


		return buildUserForAuthentication(user, authorities);
	}

	private User buildUserForAuthentication(Usuario user, List<GrantedAuthority> authorities) {
		return new User(user.getNombreUsuario(), 
				user.getPassword(), user.isActivo(), 
				true, true, true, authorities);
	}

	private List<GrantedAuthority> buildUserAuthority(Set<Roles> userRoles) {

		Set<GrantedAuthority> setAuths = new HashSet<GrantedAuthority>();

		// Build user's authorities
		for (Roles userRole : userRoles) {
			setAuths.add(new SimpleGrantedAuthority(userRole.getRol()));
		}

		List<GrantedAuthority> result = new ArrayList<GrantedAuthority>(setAuths);

		return result;
	}
}
