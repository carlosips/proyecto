package org.project.pfc.config;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebMvcSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
	CustomUserDetailService userDetailsService; 
	
	@Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .authorizeRequests()
            	.antMatchers("/inicio**").access("isFullyAuthenticated()")
            	.antMatchers("/inicio/**").access("isFullyAuthenticated()")
            	.antMatchers("/css**").access("permitAll")
            	.antMatchers("/js**").access("permitAll")
                .antMatchers("/admin/**").access("hasRole('ROLE_ADMIN')")
                .antMatchers("/operario/**").access("hasAnyRole('ROLE_OPER', 'ROLE_ADMIN')")
                .and()
            .formLogin()
                .loginPage("/login").usernameParameter("username").passwordParameter("password")
                .permitAll()
                .defaultSuccessUrl("/inicio")
                .and()
            .csrf()
                .and()
            .logout()
            	.logoutUrl("/logout")
                .permitAll()
                .and()
            .exceptionHandling().accessDeniedPage("/403");
    }


	@Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		auth.userDetailsService(userDetailsService).passwordEncoder(encoder);

    }
}
