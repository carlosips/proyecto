package org.project.pfc.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.project.pfc.conversors.*;
 
@Configuration
class WebMvcContext extends WebMvcConfigurerAdapter {
 
    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addConverter(new StringToDate());
        registry.addConverter(new StringToEdificio());
        registry.addConverter(new StringToFabricante());
        registry.addConverter(new StringToSustancias());
        registry.addConverter(new StringToEficacias());
        registry.addConverter(new StringToExtintores());
        registry.addConverter(new StringToEmpresa());
    }
}