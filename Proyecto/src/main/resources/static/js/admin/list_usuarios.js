/**
 * 
 */

$(document).ready(function() {
	$(".btnDelete").click(function() {
		var fila = $(this).parent().parent();
		var id = $(fila).attr("data-idUser");
		$.ajax({
		type: "DELETE",
		dataType : 'html',
        beforeSend: function (request)
            {
                var authorizationToken = $("meta[name='_csrf_header']").attr("content");
                var token = $("meta[name='_csrf']").attr("content"); console.log(authorizationToken + ": " + token);
                request.setRequestHeader(authorizationToken, token);
            },
		url: "/admin/borrar-usuario/" + id,
		success: function(data) {
			$(fila).remove();
		},
		error: function(data) {
			alert("HA OCURRIDO UN ERROR");
		}
		});
	})
})