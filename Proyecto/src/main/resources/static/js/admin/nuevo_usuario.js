$(document).ready(function () {
	var opciones = $("[name=\"roles_attr[]\"]");

	$.each(opciones, function(index, value) {
		$("#checkboxes").append("<label class=\"role_interface\">" +
				"<input type=\"checkbox\" id=\"" + $(value).attr("value") + "\"></input> " +  
				$(value).attr("data-rolename") + "</label>");
		
	});
	$('#checkboxes').on('change', '.role_interface', function () {
		var id = $(this).children().attr("id");

		console.log(id);
		var hidInp = $("."+id);
		console.log(hidInp);
		var isDisabled = $(hidInp).is(':disabled');
		if (isDisabled) {
			$(hidInp).prop('disabled', false);
			console.log($(hidInp).children("input"));
		} else {
			$(hidInp).prop('disabled', true);
			console.log($(hidInp).children("input"));
		}
	});
		
});