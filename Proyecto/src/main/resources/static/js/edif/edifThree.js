var renderEdif = {};
var interaction = {};

var scene, camera, renderer, control, selected, lastMaterial, pointCircle;
var selectedMaterial = new THREE.MeshPhongMaterial({color: 0xff0000, specular: 0x000000, shininess: 30});
var raycaster = new THREE.Raycaster();
var mouse = new THREE.Vector2();
var WIDTH = $("#canvas").width();
var HEIGHT = $("#canvas").height();

renderEdif.createScene = function() {
	scene = new THREE.Scene();
	//scene.overrideMaterial = new THREE.MeshDepthMaterial();
    
    
    var light = new THREE.PointLight(0xffffff);
    light.position.set(50,30,50);
    //scene.add(light);
    
    var ambientlight = new THREE.AmbientLight( 0x222222 );
    scene.add( ambientlight );
    
    var fileName = $("#info").attr("data-wavefront");
    var loader = new THREE.OBJLoader();
    
    // load a resource
    loader.load(
    		// resource URL
    		'/models/'+fileName,
    		// Function when resource is loaded
    		function ( object ) {
    			scene.add( object );
    		}
    );
    

    var mycanvas = document.getElementById("canvas");
    renderer = new THREE.WebGLRenderer({ antialias: true });
	renderer.setSize( WIDTH, HEIGHT );
	
	camera = new THREE.PerspectiveCamera(45, WIDTH / HEIGHT, 0.1, 20000);
    camera.position.set(50,50,50);
    camera.lookAt(scene.position);
    
    controls = new THREE.OrbitControls( camera, mycanvas );
    controls.addEventListener( 'change', renderEdif.renderScene );
    
    camera.add(light);
    
    scene.add(camera);
	
    var geometry = new THREE.SphereGeometry( 0.4, 18, 18 );
    var material = new THREE.MeshBasicMaterial( {color: 0xffff00} );
    pointCircle = new THREE.Mesh( geometry, material );
    scene.add( pointCircle );
    pointCircle.visible = false;
    
	$("#canvas").append(renderer.domElement);	
	
}

renderEdif.animateScene = function() {
	
	requestAnimationFrame( renderEdif.animateScene );
	// update the picking ray with the camera and mouse position	
	
	
	renderer.render(scene, camera);
}

renderEdif.renderScene = function() {
	

	renderer.render(scene, camera);
}

renderEdif.onMouseDown = function ( event ) {

	// calculate mouse position in normalized device coordinates
	// (-1 to +1) for both components
	var pos = $("canvas").offset();
	
	mouse.x = ( (event.clientX - pos.left) / WIDTH ) * 2 - 1;
	mouse.y = - ( (event.clientY - pos.top) / HEIGHT ) * 2 + 1;		

	// update the picking ray with the camera and mouse position	
	raycaster.setFromCamera( mouse, camera );	

	// calculate objects intersecting the picking ray
	var intersects = raycaster.intersectObjects( scene.children, true );
	//console.log(intersects);
	if (lastMaterial != null && typeof(lastMaterial) != undefined) {
		selected.material = lastMaterial;
	}
	 
	if (intersects.length > 0) {
		var index = 0; 
		while (intersects[ index ].object == pointCircle || index > intersects.length) {
			index = index+1;
		}
		if (index < intersects.length) {
			lastMaterial = intersects[ index ].object.material;
			selected = intersects[ index ].object;
			pointCircle.position.setX(intersects[ index ].point.x);
			pointCircle.position.setY(intersects[ index ].point.y);
			pointCircle.position.setZ(intersects[ index ].point.z);
			$("#cordenadaX").val(Math.round(intersects[ index ].point.x*100)/100);
			$("#cordenadaY").val(Math.round(intersects[ index ].point.y*100)/100);
			$("#cordenadaZ").val(Math.round(intersects[ index ].point.z*100)/100);
			selected.material = selectedMaterial;
			openNodeDataGuid(selected.name);
		}
	} else {
		selected.material = lastMaterial;
		selected = null;
		lastMaterial = null;
	}
	
}

renderEdif.test = function (object) {
	var bbox = new THREE.BoundingBoxHelper( object, 0xffaa00);
	bbox.update();
	scene.add( bbox );
}

interaction.actionChildren = function(node, callbck) {
	if (interaction.isCallback(callbck)) {
		callbck(node);
	}
	for (var i = 0; i<node.children.length; i++) {
		var child = $.jstree.reference($("#ifcTree")).get_node(node.children[i]);
		interaction.actionChildren(child, callbck);
	}
}

interaction.isCallback = function(obj) {
	if (obj != null && typeof obj === 'function')
		return true;
	else
		return false;
}

//Uso de las librerias Three.js y jstree.js
$(document).ready(function() {
	renderEdif.createScene();
	renderEdif.animateScene();
	//window.addEventListener( 'mousedown', renderEdif.onMouseDown, false );
	$("#canvas").on("dblclick", renderEdif.onMouseDown);
	reestructureTree();

	$("#ifcTree").jstree({
		"plugins" : [ "wholerow", "changed", "contextmenu" ],
		"contextmenu" : {
			"items" : function(o) {
				var options = {
						"hide": {
							"separator_before" 	: false,
							"separator_after"	: false,
							"_disabled"			: false,
							"label"				: "Ocultar/Mostrar",
							"action"			: function(obj) {
								var node = $.jstree.reference($("#ifcTree")).get_node(obj.reference.prevObject.selector);
								
								interaction.actionChildren(node, function(node) {
									var guid = node.data.guid;
									if (guid != null && typeof(guid) != undefined) {
										if (scene.getObjectByName(guid)) {
											if (scene.getObjectByName(guid).visible)
												scene.getObjectByName(guid).visible=false;
											else
												scene.getObjectByName(guid).visible=true;
										}
									}
								});
								
							}
						},
						"select": {
							"separator_before" 	: false,
							"separator_after"	: false,
							"_disabled"			: function(obj) {
								
								if($.jstree.reference($("#ifcTree")).is_leaf(obj.reference.prevObject.selector)) {
									return false;
								}
								
								return true;
							},
							"label"				: "Seleccionar",
							"action"			: function(obj) {
								var node = $.jstree.reference($("#ifcTree")).get_node(obj.reference.prevObject.selector);
								interaction.actionChildren(node, function(node) {
									var guid = node.data.guid;
									if (guid != null && typeof(guid) != undefined) {
										var modelObj = scene.getObjectByName(guid);
										if (modelObj != null && typeof(modelObj) != "undefined") {
											camera.position = modelObj.position;
											camera.position.setY(camera.position.y + 5);
											//camera.lookAt(modelObj.position);
											if (selected !== null && typeof(selected) !== "undefined" && 
													lastMaterial !==  null && typeof(lastMaterial) !== undefined) {
												
												selected.material = lastMaterial;
											}
											lastMaterial = modelObj.material;
											selected = modelObj;
											selected.material = selectedMaterial;
										}
									}
								});
							}
						},
						"exting_info": {
							"separator_before" 	: false,
							"separator_after"	: false,
							"_disabled"			: function(obj) {
								var node = $.jstree.reference($("#ifcTree")).get_node(obj.reference.prevObject.selector);
								if($.jstree.reference($("#ifcTree")).is_leaf(obj.reference.prevObject.selector) &&
										node.data.type == "ExtinguisherUnit") {
									return false;
								}
								
								return true;
							},
							"label"				: "Información",
							"action"			: function(obj) {
								var node = $.jstree.reference($("#ifcTree")).get_node(obj.reference.prevObject.selector);
								$('input[name="ifcGlobalId"]').val(node.data.guid);
								if ($("#extintorInfo").length) {
									$.ajax({
										  method: "GET",
										  url: "/ajax/extintor",
										  data: { edif: $('#extintorInfo').attr("data-edif"), guid: node.data.guid }
										})
										.done(function(data) {
										$("#l_numRegistro").text(data.numRegistro);
										$("#l_fabricante").text(data.fabricante.nombre);
										$("#l_fechaFabricacion").text(showDate(data.fechaFabricacion));
										$("#l_cargaKg").text(data.carga);
										$("#l_presionRetimbrado").text(data.presionRetimbrado);
										$("#l_sustancias").text(data.sustancias.nombre);
										$("#_eficacias_A").hide();
										$("#_eficacias_B").hide();
										$("#_eficacias_F").hide();
										$('#extintorInfo').attr("data-extId", data.id);
										data.eficaciasExtintors.forEach(function(eficacia){
											if (eficacia.eficacias.tipo == "A"){
												$("#l_eficacia_A").text(eficacia.eficacias.nombre);
												$("#_eficacias_A").show();
											}
											if (eficacia.eficacias.tipo == "B"){
												$("#l_eficacia_B").text(eficacia.eficacias.nombre);
												$("#_eficacias_B").show();
											}
											if (eficacia.eficacias.tipo == "F"){
												$("#l_eficacia_F").text(eficacia.eficacias.nombre);
												$("#_eficacias_F").show();
											}
										});
										$("#mant-date").children().remove();
										$("#mant-date").append("<ul></ul>");
										setFormExtintor(data); //declarado en edificio.js
										data.mantenimientos.forEach(function(mantenimiento){
											var item = $("<li><a href=\"#\">"+showDate(mantenimiento.fecha)+"</a></li>");
											$(item).attr("data-fecha", showDate(mantenimiento.fecha));
											$(item).attr("data-usuario", mantenimiento.usuario.nombreReal);
											$(item).attr("data-descripcion", mantenimiento.descripcion);
											if (mantenimiento.porFabricante)
												$(item).attr("data-empresa", "fabricante");
											else
												$(item).attr("data-empresa", mantenimiento.empresa.nombre);
											
											$("#mant-date ul").append(item);
										});
									});
									$('#extintorInfo').modal();
								} else {
									$('#myModal').modal();
								}

							}
						}
				};

				return options;
			}
		}
	});
		
});


function reestructureTree() {
	var flowTerminal = $("[data-type=\"FlowTerminal\"]");
	for (var i = 0; i<flowTerminal.length; i++) {
		var listext = $(flowTerminal[i]).children("ul")[0];
		var extinguishers = $(listext).children("[data-type=\"ExtinguisherUnit\"]");
		var exul = $("<ul></ul>");
		for(var j = 0; j<extinguishers.length; j++) {
			exul.append(extinguishers[j]);
		}
		var exli = $("<li><span>Extinguisher</span></li>");
		exli.append(exul);
		$(exli).insertBefore($(flowTerminal[i]));
	}
}


function openNodeDataGuid(dataGuid) {
	$("#ifcTree").jstree("open_all");
	var node = $("#ifcTree").jstree("get_node", '[data-guid="'+dataGuid+'"]');
	console.log(node);
	$("#ifcTree").jstree("close_all");
	
	node.parents.forEach(function(elem){
		console.log(elem);
		var parent = $("#ifcTree").jstree("get_node", elem);
		$("#ifcTree").jstree("open_node", parent);
	});
	$("#ifcTree").jstree("deselect_all");
	$("#ifcTree").jstree("select_node", node);
}