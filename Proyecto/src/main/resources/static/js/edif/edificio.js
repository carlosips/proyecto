/*var edificio = {};

edificio.altaExtintor = function() {
	
}*/
var sust_A = [];
var sust_B = [];
var sust_F = [];
$(document).ready( function() { 
$('#myModal').on('shown.bs.modal', function () {
  $('#myInput').focus()
});

$.ajax('/ajax/eficacias/A')
.done(function(data) {
	$('#eficaciasExtintorsA').children("option").remove();
	$.each(data, function(i, item) {
		$('#eficaciasExtintorsA').append("<option value=\""+data[i].nombre+"\">"+data[i].nombre+"</option>")
	});
	$('#eficaciasExtintorsAAdd').children("option").remove();
	$.each(data, function(i, item) {
		$('#eficaciasExtintorsAAdd').append("<option value=\""+data[i].nombre+"\">"+data[i].nombre+"</option>")
	});
});

$.ajax('/ajax/eficacias/B')
.done(function(data) {
	$('#eficaciasExtintorsB').children("option").remove();
	$.each(data, function(i, item) {
		$('#eficaciasExtintorsB').append("<option value=\""+data[i].nombre+"\">"+data[i].nombre+"</option>")
	});
	$('#eficaciasExtintorsBAdd').children("option").remove();
	$.each(data, function(i, item) {
		$('#eficaciasExtintorsBAdd').append("<option value=\""+data[i].nombre+"\">"+data[i].nombre+"</option>")
	});
});

$.ajax('/ajax/eficacias/F')
.done(function(data) {
	$('#eficaciasExtintorsF').children("option").remove();
	$.each(data, function(i, item) {
		$('#eficaciasExtintorsF').append("<option value=\""+data[i].nombre+"\">"+data[i].nombre+"</option>")
	});
	$('#eficaciasExtintorsFAdd').children("option").remove();
	$.each(data, function(i, item) {
		$('#eficaciasExtintorsFAdd').append("<option value=\""+data[i].nombre+"\">"+data[i].nombre+"</option>")
	});
});

$.ajax('/ajax/fabricantes')
.done(function(data) {
	$('#fabricante').children("option").remove();
	$.each(data, function(i, item) {
		$('#fabricante').append("<option value=\""+data[i].id+"\">"+data[i].nombre+"</option>")
	});
	$('#fabricanteAdd').children("option").remove();
	$.each(data, function(i, item) {
		$('#fabricanteAdd').append("<option value=\""+data[i].id+"\">"+data[i].nombre+"</option>")
	});
});

$.ajax('/ajax/sustancias')
.done(function(data) {
	$('#sustancias').children("option").remove();
	$('#sustancias').append("<option selected></option>")
	$.each(data, function(i, item) {
		addSustanciaType(data[i]);
		$('#sustancias').append("<option value=\""+data[i].nombre+"\">"+data[i].nombre+"</option>")
	});
	$('#sustanciasAdd').children("option").remove();
	$('#sustanciasAdd').append("<option selected></option>")
	$.each(data, function(i, item) {
		addSustanciaType(data[i]);
		$('#sustanciasAdd').append("<option value=\""+data[i].nombre+"\">"+data[i].nombre+"</option>")
	});
});


$('#sustancias').on('change', function() {
	$("#eficacias_A").hide();
	$("#eficacias_B").hide();
	$("#eficacias_F").hide();
	$("#eficaciasExtintorsA").prop("disabled", true);
	$("#eficaciasExtintorsB").prop("disabled", true);
	$("#eficaciasExtintorsF").prop("disabled", true);
	if (sust_A.indexOf(this.value) != -1) {
		$("#eficacias_A").show();
		$("#eficaciasExtintorsA").prop("disabled", false);
	}
	if (sust_B.indexOf(this.value) != -1) {
		$("#eficacias_B").show();
		$("#eficaciasExtintorsB").prop("disabled", false);
	}
	if (sust_F.indexOf(this.value) != -1) {
		$("#eficacias_F").show();
		$("#eficaciasExtintorsF").prop("disabled", false);
	}
});

$('#sustanciasAdd').on('change', function() {
	$("#eficacias_A_Add").hide();
	$("#eficacias_B_Add").hide();
	$("#eficacias_F_Add").hide();
	$("#eficaciasExtintorsAAdd").prop("disabled", true);
	$("#eficaciasExtintorsBAdd").prop("disabled", true);
	$("#eficaciasExtintorsFAdd").prop("disabled", true);
	if (sust_A.indexOf(this.value) != -1) {
		$("#eficacias_A_Add").show();
		$("#eficaciasExtintorsAAdd").prop("disabled", false);
	}
	if (sust_B.indexOf(this.value) != -1) {
		$("#eficacias_B_Add").show();
		$("#eficaciasExtintorsBAdd").prop("disabled", false);
	}
	if (sust_F.indexOf(this.value) != -1) {
		$("#eficacias_F_Add").show();
		$("#eficaciasExtintorsFAdd").prop("disabled", false);
	}
});

$('#form-extintor').on("submit", function(e){
	var postData = $(this).serialize();
	var action = $(this).attr("action");
	$.ajax({
		type: "POST",
		url: action,
		data: $(this).serialize(),
		success: function() {
			location.reload();
		}
	})
	e.preventDefault(); //STOP default action
});

$("#mantButton").on("click", function(e) {
	var id = $('#extintorInfo').attr("data-extId");
	$("[name=\"extintores\"]").val(id);
	$('#extintorInfo').modal('toggle');
	$('#extintorMant').modal();
});

$("#editButton").on("click", function(e) {
	var id = $('#extintorInfo').attr("data-extId");
	var action_id = $("#form-extintor").attr("action") + id;
	$("#form-extintor").attr("action", action_id);
	$('#extintorInfo').modal('toggle');
	$('#editModal').modal();
});

$("#mant-fabricante").on("click", function(e){
	if ($(this).prop("checked")) {
		$("#mant-empresa").prop("disabled", true);
	} else {
		$("#mant-empresa").prop("disabled", false);
	}
});

$('#form-mantenimiento').on("submit", function(e){
	var postData = $(this).serialize();
	var action = $(this).attr("action");
	$.ajax({
		type: "POST",
		url: action,
		data: $(this).serialize(),
		success: function() {
			$('#extintorMant').modal('toggle');
		}
	})
	e.preventDefault(); //STOP default action
});

$("#mant-date").on("click", "li", function() {
	$(".list-selected").removeClass("list-selected");
	$("#l_mant_user").text($(this).attr("data-usuario"));
	$("#l_mant_fecha").text($(this).attr("data-fecha"));
	$("#l_mant_emp").text($(this).attr("data-empresa"));
	$("#l_mant_descrip").text($(this).attr("data-descripcion"));
	$(this).addClass("list-selected");
});

$("#updatePoint").on("click", function() {
	pointCircle.position.setX($("#cordenadaX").val());
	pointCircle.position.setY($("#cordenadaY").val());
	pointCircle.position.setZ($("#cordenadaZ").val());
});

$("#showPoint").on("click", function() {
	if (pointCircle.visible) {
		pointCircle.visible = false;
	}else{
		pointCircle.visible = true;
	}
});

$("#uploadExt").on("click", function(e) {
	var cordX = $('#cordenadaX').val();
	var cordY = $('#cordenadaY').val();
	var cordZ = $('#cordenadaZ').val();
	console.log(cordX); console.log(cordY); console.log(cordZ);
	if (cordX == null || cordY == null || cordZ == null || selected == null || typeof selected == undefined) {
		alert("Se debe seleccionar un punto y un objeto de referencia");
		return;
	} else {
		$('#form-add-extintor [name="cordX"]').val(cordX);
		$('#form-add-extintor [name="cordY"]').val(cordY);
		$('#form-add-extintor [name="cordZ"]').val(cordZ);
		$('#form-add-extintor [name="referenceGuid"]').val(selected.name);
	}

	$('#addExt').modal();
});

$('#deleteButton').on("click", function(e) {
	var id = $('#extintorInfo').attr("data-extId");
	$("[name=\"idExt\"]").val(id);
	$('#extintorInfo').modal('toggle');
	$('#extintorDelete').modal();
});



});



function addSustanciaType(obj) {
	if (obj.tipo != null && typeof obj.tipo != undefined) {
		if (obj.tipo.search("A") != -1) {
			sust_A.push(obj.nombre);
		}
		if (obj.tipo.search("B") != -1) {
			sust_B.push(obj.nombre);
		}
		if (obj.tipo.search("F") != -1) {
			sust_F.push(obj.nombre);
		}
	}
}

function setFormExtintor(extintor) {
	$('#form-extintor [name="numRegistro"]').val(extintor.numRegistro);
	$('#form-extintor [name="fabricante"] option[value="'+ extintor.fabricante.id +'"]').attr("selected", "selected");
	$('#form-extintor [name="fechaFabricacion"]').val(extintor.fechaFabricacion);
	$('#form-extintor [name="carga"]').val(extintor.carga);
	$('#form-extintor [name="presionRetimbrado"]').val(extintor.presionRetimbrado);
	$('#form-extintor [name="sustancias"] option[value="'+ extintor.sustancias.nombre +'"]').attr("selected", "selected");
	extintor.eficaciasExtintors.forEach(function(eficaciaExtintor) {
		var tipo = eficaciaExtintor.eficacias.tipo;
		$("#eficacias_"+tipo).show();
		$("#eficaciasExtintors"+tipo).prop("disabled", false);
		$('#eficaciasExtintors'+tipo+' option[value="'+ eficaciaExtintor.eficacias.nombre +'"]').attr("selected", "selected");
	})
}

function showDate(date) {
	var pattern = /(\d{4})\-(\d{2})\-(\d{2})/;
	return date.replace(pattern,'$3-$2-$1');
}